# Embedded application using Gradle, Jersey and Jetty #

In your terminal find the appplication path and:

### Build and create the war file: ###
gradle build

### Run application ###
java -jar jetty-runner-9.0.0.M4.jar --port 9180 build/libs/embedded-gradle-jersey-jetty.war 